import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './containers/game/game.component';
import { StartPageComponent } from './containers/start-page/start-page.component';	

const routes: Routes = [
	{ path: ':game', component: GameComponent },
	{ path: '', component: StartPageComponent },
	{ path: '', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
