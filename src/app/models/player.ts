import { card } from './card';

export interface Player {
	name: string,
	status: boolean,
	host: boolean,
	stick: number,
	cards: card[]
}