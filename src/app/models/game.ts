import { Player } from './player';

export class Game {

	constructor( 
		public name: string = '',
		public numberOfPlayers: number = 0,
		public gameNr: number = 0,
		public round: number = 0,
		public players: Player[] = [],
		public dealer: number = -1,
		public playerTurn: number =	0
	) {}

}