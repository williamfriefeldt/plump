export interface card {
	house: string;
	value: string;
	image: string;
	posLeft: number;
}