import { Injectable } from '@angular/core';
import { gameStatus } from '../models/game-status';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Game } from '../models/game';
import { Player } from '../models/player';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})
export class GameService {

	game: Game;
  player: Player;
  host: boolean = false;

  constructor(
    private fb: FirebaseService
  ) { }

  setGame( gameName: string ) : Promise<void>{
    this.game = new Game();
  	this.game.name = gameName;
    return this.fb.addGame( this.game ).then( () => console.log('Lägger till spel') );
  }

  checkGame( gameName: string ) : Observable<any> {
    const allGames = this.fb.getAllGames().pipe(
      map(
       (res:any) => {
        console.log('Hämtar alla spel')
        const isGame = res.find( (game:any) => game.payload.val().name === gameName );
        if(isGame !== undefined) {
          this.game = new Game();
          this.game.name = gameName;
        }
        return isGame;
      }));
     return allGames;
  }

  checkPlayer( playerName: string ) : Observable<any> {
    const allPlayers = this.fb.getPlayers( this.game.name ).pipe(
      map(
        (res:any) => {
          let isPlayer;
          if( res.length === 0 ) {
            this.host = true;
            isPlayer = undefined;
          } else {
            isPlayer = res.find( (player:any) => player.payload.val().name === playerName );
          }
          return isPlayer;
      }));
    return allPlayers;
  }

  addPlayer( playerName:string ) : Promise<void> {
    this.player = {
      name: playerName,
      status: true,
      host: this.host,
      stick: 0,
      cards: []
    }
    return this.fb.addPlayer( this.game.name, this.player).then( res => console.log('Lägger till spelare') );
  } 

  fetchGame() : Observable<any> {
    const game = this.fb.getGame( this.game.name ).pipe( 
      map( (res:any) => {
        console.log('Hämtar spelet: ' + this.game.name );
        console.log(res);
        this.game.dealer = res[0];
        this.game.gameNr = res[1];
        this.game.name = res[2];
        this.game.numberOfPlayers = res[3];
        this.game.playerTurn = res[4];
        this.game.players = res[5];
        this.game.round = res[6];

        this.player = res[5][this.player.name];

     }));
    return game;

  }

  getGame() : Game {
    return this.game;
  }

  getPlayers() : Player[] {
    return this.game.players;
  }

  getPlayer() : Player {
    return this.player;
  }

  getGameStatus( gameNr: number ) : string {
    switch( gameNr ) {
      case 0:
  	    return gameStatus.waiting;
      case 1:
        return gameStatus.gameOne;
      default:
        return '';
    }
  }

}
