import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Game } from '../models/game';
import { Player } from '../models/player';

import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(
  	private db: AngularFireDatabase
  ) {}

  addGame( game: Game ): Promise<void> {
  	return this.db.list('/').set( game.name, game);
  }

  getAllGames() : Observable<any>{
  	return this.db.list('/').snapshotChanges();
  }

  getGame( gameName: string ) : Observable<any>{
    return this.db.list('/' + gameName ).valueChanges();
  }

  removeGame( gameName: string ) : Promise<void>{
    return this.db.list('/' + gameName ).remove();
  }

  getPlayers( gameName: string ) : Observable<any>{
    return this.db.list('/' + gameName + '/players').snapshotChanges();
  }

  changePlayerStatus( gameName:string, player: Player ) : Promise<void> {
    return this.db.list('/' + gameName + '/players/').set( player.name, player );
  }

  changeGameStatus( game: Game ) : Promise<void> {
    return this.db.list('/').set( game.name, game );
  }

  addPlayer( gameName:string, player: Player) : Promise<void> {
    return this.db.list('/' + gameName + '/players').set( player.name, player );
  }

  updateGameNr( number: number, game: Game) : Promise<void> {
    game.gameNr = number;
    if( number === 1 ) {
      game.dealer = Object.values(game.players).length - 1;
    } else {
      game.dealer = game.dealer - 1;
    }  
    return this.db.list('/' ).set( game.name, game );
  }

  updateRoundNr(number: number, game: Game): Promise<void>{
    game.round=number;
    return this.db.list('/' ).set(game.name , game );
  }
}
