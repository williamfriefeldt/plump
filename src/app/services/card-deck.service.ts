import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { card } from '../models/card';

@Injectable({
  providedIn: 'root'
})
export class CardDeckService {

	apiUrl : string = "https://deckofcardsapi.com/api/deck/";
	deck_id : string = '';
	cards : card[] = [];

  constructor(
  	private http: HttpClient
  ) { }

  setCardDeckId() : Observable<object> {
  	return this.http.get( this.apiUrl + "new/shuffle/?deck_count=1" ).pipe(
  			map( (res:any) => this.deck_id = res.deck_id )
  		);
  }

  drawCards( nrOfCards:number ) : Observable<card> {
    let positionLeft = -200;
  	return this.http.get( this.apiUrl + this.deck_id + "/draw/?count=" + nrOfCards ).pipe(
  			map( (res:any) => this.cards = res.cards.map( (card:any) => {
          positionLeft += 25;
  				return {
  					house: card.suit,
  					value: card.value,
  					image: card.image,
            posLeft: positionLeft
  				}
  			}))
  		);
  }

  getDeckId() : string {
  	return this.deck_id;
  }

  getCards() : card[] {
  	return this.cards;
  }

}
