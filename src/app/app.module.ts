import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';

import { environment } from '../environments/environment';
import { StartPageComponent } from './containers/start-page/start-page.component';
import { GameComponent } from './containers/game/game.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateGameComponent } from './components/create-game/create-game.component';

import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';

import { JoinGameComponent } from './components/join-game/join-game.component';
import { NoGameComponent } from './components/no-game/no-game.component';
import { PlayerNameComponent } from './components/player-name/player-name.component';
import { PlayersComponent } from './components/players/players.component';
import { GameStatusComponent } from './components/game-status/game-status.component';
import { HostComponent } from './components/host/host.component';
import { PlayerCardsComponent } from './components/player-cards/player-cards.component';
import { PlayerControlComponent } from './containers/player-control/player-control.component';
import { PickStickComponent } from './components/pick-stick/pick-stick.component';

@NgModule({
  declarations: [
    AppComponent,
    StartPageComponent,
    GameComponent,
    CreateGameComponent,
    JoinGameComponent,
    NoGameComponent,
    PlayerNameComponent,
    PlayersComponent,
    GameStatusComponent,
    HostComponent,
    PlayerCardsComponent,
    PlayerControlComponent,
    PickStickComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    FormsModule,
    BrowserAnimationsModule,
    MatExpansionModule, 
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
