import { Component, OnInit, Input} from '@angular/core';
import { Player } from '../../models/player'

@Component({
  selector: 'app-pick-stick',
  templateUrl: './pick-stick.component.html',
  styleUrls: ['./pick-stick.component.scss']
})
export class PickStickComponent implements OnInit {

	@Input() players : Player[] = [];
	@Input() dealer : number = -1;
	@Input() playerTurn : number;

  constructor() { }

  ngOnInit(): void {
  }

}
