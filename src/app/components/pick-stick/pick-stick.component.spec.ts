import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PickStickComponent } from './pick-stick.component';

describe('PickStickComponent', () => {
  let component: PickStickComponent;
  let fixture: ComponentFixture<PickStickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PickStickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PickStickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
