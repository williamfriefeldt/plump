import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Game } from '../../models/game';
import { FormsModule } from '@angular/forms';

import { GameService } from '../../services/game.service';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.scss']
})
export class CreateGameComponent implements OnInit {

  gameName: string = '';

  constructor(
  	private gameService: GameService,
  	private router: Router
  ) { }

  ngOnInit(): void { }

  addGame(): void {
    console.log(this.gameName);
  	this.gameService.setGame( this.gameName ).then( () => {
  		this.router.navigate(['/' + this.gameName]);
  	});
  } 

}
