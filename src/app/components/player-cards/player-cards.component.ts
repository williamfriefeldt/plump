import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CardDeckService } from '../../services/card-deck.service';
import { card } from '../../models/card';

@Component({
  selector: 'app-player-cards',
  templateUrl: './player-cards.component.html',
  styleUrls: ['./player-cards.component.scss']
})
export class PlayerCardsComponent implements OnInit {

  chosenCard : card;

  @Input() cards: card[] = [];

  @Output() chosenCardEvent = new EventEmitter<card>();

  constructor(
  	private deckService: CardDeckService
  ) { }

  ngOnInit(): void { }

  chooseCard( card: card ) : void {
    this.chosenCard = card;
    this.chosenCardEvent.emit(this.chosenCard);
  }
}
