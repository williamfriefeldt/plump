import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-no-game',
  templateUrl: './no-game.component.html',
  styleUrls: ['./no-game.component.scss']
})
export class NoGameComponent implements OnInit {

	@Input() name : string;

  constructor(
  	private router: Router
  ) { }

  ngOnInit(): void { }

  back(): void {
    this.router.navigate(['/']);
  }

}
