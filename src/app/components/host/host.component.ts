import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CardDeckService } from '../../services/card-deck.service';
import { GameService } from '../../services/game.service';
import { FirebaseService } from '../../services/firebase.service';
import { Player } from '../../models/player';
import { card } from '../../models/card';

@Component({
  selector: 'app-host',
  templateUrl: './host.component.html',
  styleUrls: ['./host.component.scss']
})
export class HostComponent implements OnInit {
  @Input() gameNr!: number;
  @Output() startGameEvent = new EventEmitter<number>();
  @Output() startRoundEvent = new EventEmitter<number>();

  players: Player[];

  constructor(
    private deckService : CardDeckService,
    private gameService : GameService,
    private fb : FirebaseService
  ) { }

  ngOnInit(): void {
  }

  startGame(): void {
    this.players = this.gameService.getPlayers();
    const setIdCall = this.deckService.setCardDeckId().subscribe( () => {
      console.log('Hämtar nyckel');

      const playersValues = Object.values(this.players);

      playersValues.forEach( (player, index) => {
        const getCards = this.deckService.drawCards( 10 ).subscribe( () => {
          player.cards = this.deckService.getCards();
          if( index === playersValues.length - 1 ) {
            const game = this.gameService.getGame();
            game.players = this.players;
            console.log(this.players);
            this.fb.changeGameStatus( game ).then( () => {
              this.startGameEvent.emit(1);
            });
          }
          getCards.unsubscribe();
        });
      });

      setIdCall.unsubscribe();

    });
  }

  startRound(): void{
    this.startRoundEvent.emit(1);

  }

}
