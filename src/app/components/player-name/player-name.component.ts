import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-player-name',
  templateUrl: './player-name.component.html',
  styleUrls: ['./player-name.component.scss']
})
export class PlayerNameComponent implements OnInit {

	@Output() playerNameEvent = new EventEmitter<string>();
	playerName: string;

  constructor() { }

  ngOnInit(): void {
  }

  joinGame(): void {
  	this.playerNameEvent.emit(this.playerName);
  }

}
