import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FirebaseService } from '../../services/firebase.service';
import { GameService } from '../../services/game.service';

import { Game } from '../../models/game';
import { Player } from '../../models/player';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {

  /**
    @description If player close window, set player status to false and check if any 
    players are left in the game.
  */
  @HostListener('window:unload') unloadHandler() {
    this.player.status = false;
    this.fb.changePlayerStatus( this.gameName, this.player ).then( res => {
      this.checkNumberOfPlayers();
    });
  }  

	gameName: string = '';
  player: Player;
  game: any;
  players: Player[];
  gameStatus: string;
  isPlayerTurn: boolean =  false;

	noGame: boolean = false;
  loading: boolean = true;

  constructor(
  	private router: ActivatedRoute,
  	private fb: FirebaseService,
    private currentRouter: Router,
    private gameService: GameService
  ) { }

  /**
    @description Get game name from URL and check if game exists.
  */
  ngOnInit(): void {

  	this.gameName = this.router.snapshot.paramMap.get( 'game' ) as string;

    const checkGame = this.gameService.checkGame( this.gameName ).subscribe(res => {
      if( res === undefined ) {
        this.noGame = true;  
      }
      checkGame.unsubscribe();
      this.loading = false;  
    });

  }

  /**
    @description Check if player exists. If not, add player to the game.
    The call 'fetchGame()' will track ALL changes in the database.
  */
  addPlayerName( playerName: string ): void {
    const checkPlayer = this.gameService.checkPlayer( name ).subscribe( res => {
      if( res === undefined ) this.gameService.addPlayer( playerName ).then( () => {
        const game = this.gameService.fetchGame().subscribe( res => {
          console.log('Hämtar spelet från service');
          this.game = this.gameService.getGame();
          if( typeof this.game.name !== 'string' ) {
            game.unsubscribe();
            return;
          }
          this.players = Object.values(this.game.players);
          this.player = this.gameService.getPlayer();
          const playerIndex = this.players.findIndex( player => player.name === this.player.name );
          if( playerIndex === this.game.playerTurn ) {
            this.isPlayerTurn = true;
          } else {
            this.isPlayerTurn = false;
          }
          this.gameStatus = this.gameService.getGameStatus( this.game.gameNr );
        });
      });
      checkPlayer.unsubscribe();
    })
  }

  /**
    @description If player exit the game, set player status to false 
    and check if any players are left in the game.
  */
  ngOnDestroy(): void {
    if(!this.noGame) {
      if(this.player === undefined) {
        this.currentRouter.navigate(['/']);
      } else {
        this.player.status = false;
        this.fb.changePlayerStatus( this.gameName, this.player ).then( () => {
          this.checkNumberOfPlayers();
        });
      }
    }
  }

  checkNumberOfPlayers(): void {
    this.fb.getPlayers( this.gameName ).subscribe( res => {
      const onlinePlayers = res.filter( (player:any) => player.status === true );
      if(onlinePlayers.length === 0) this.fb.removeGame( this.gameName );
    });
  }

  updateGameNr( number: number ) : void {
    this.fb.updateGameNr( number, this.game );
  }
  updateRoundNr( number: number ) : void {
    this.fb.updateRoundNr( number, this.game );
  }

  addPlayerStick( stickNr: number ) : void {
    this.player.stick = stickNr;
    this.fb.changePlayerStatus( this.gameName, this.player).then( () => {
      //if(this.players.length-1>this.game.playerTurn){
        this.game.playerTurn = this.game.playerTurn + 1;
      //}
      //else{
        //Start game on table
      //  this.game.round++;
      //}
      this.fb.changeGameStatus( this.game ).then( res => console.log('Uppdaterat spelarens tur'));
    });
  }

}
