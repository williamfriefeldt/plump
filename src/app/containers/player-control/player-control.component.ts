import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { card } from '../../models/card';

@Component({
  selector: 'app-player-control',
  templateUrl: './player-control.component.html',
  styleUrls: ['./player-control.component.scss']
})
export class PlayerControlComponent implements OnInit {

	stickNr: number = 0;
	chosenCard: card;

	@Input() round : number = 0;
	@Input() isPlayerTurn : boolean = false;
  @Input() cards : card[] = [];

  @Output() addPlayerStickEvent = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void { }

  addStick() : void {
    console.log('Antal stick:' + this.stickNr);
    this.addPlayerStickEvent.emit( this.stickNr );
  }

}
