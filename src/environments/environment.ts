// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCAiP6yXoRaWYxJWKxh79nALP7Jlo0Ttbk",
    authDomain: "plump-5cdd3.firebaseapp.com",
    databaseURL: "https://plump-5cdd3.firebaseio.com",
    projectId: "plump-5cdd3",
    storageBucket: "plump-5cdd3.appspot.com",
    messagingSenderId: "609091125609",
    appId: "1:609091125609:web:83b8f89371ae00549dfdf8",
    measurementId: "G-QRW461L3RX"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
