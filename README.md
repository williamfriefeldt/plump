# Plump

This is a card game called [Plump](https://www.spelregler.org/plump-regler/) created as web game with
[Angular CLI 11](https://github.com/angular/angular-cli), [Angular Material](https://material.angular.io/) for UI and [Firebase](https://firebase.google.com/) as database.

## Start app

1. If you dont have a SSH key, [generate one](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/).
2. Clone the repository into a choosen folder.
`git clone git@bitbucket.org:williamfriefeldt/plump.git`
3. Run `npm install`
4. Run `npm install -g @angular/cl`
5. Run `ng serve`
6. Navigate to `http://localhost:4200`
7. Voila! Start coding :sunglasses:


## Create component or service

###Component
Run `ng generate component components/name-of-component` to generate a new component.

###Service
Run `ng generate service services/name-of-service` to generate a new service.

## Further help

Ask William :wink:
Philip was here